import React, { useState } from "react";

import "../Navigation/Navigation.css";

import logo from "../../logo.svg";
import dark from "../../switch-theme__dark.svg";
import light from "../../switch-theme__light.svg";

import { Link } from "react-router-dom";
import { Container, Navbar, Nav } from "react-bootstrap";

function Navigation() {
  const [theme, setTheme] = useState("light");
  const switchTheme = () => {
    if (theme === "light") {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  };

  const [toggle, setToggle] = useState("close");

  const switchToggle = () => {
    if (toggle === "close") {
      setToggle("open");
    } else {
      setToggle("close");
    }
  };
  return (
    <div className="navigation">
      <Container fluid>
        <Navbar collapseOnSelect expand="md">
          <Link to="/" className="navbar-brand navigation__logo">
            <img src={logo} alt="Covid-19 DZ Logo" />
          </Link>
          <Navbar.Toggle
            onClick={switchToggle}
            aria-controls="responsive-navbar-nav"
          />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">
              <Link to="/" className="nav-link active">
                Accueil
              </Link>
              <Link to="/donate" className="nav-link">
                Contribuer
              </Link>
              <Link
                to="#"
                className="navigation__themeSwitch"
                onClick={switchTheme}
              >
                <img
                  src={theme === "light" ? dark : light}
                  alt={
                    theme === "light"
                      ? "Switch to Dark theme"
                      : "Switch to Light theme"
                  }
                />
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </div>
  );
}

export default Navigation;
