import "./App.css";

import { Navigation, Footer } from "./component";
import Home from "./Home";

import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="app">
      <Navigation />
      <Switch>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
